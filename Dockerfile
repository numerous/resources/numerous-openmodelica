FROM openmodelica/openmodelica:v1.19.2-ompython

RUN apt-get update && \
      apt-get -y install sudo

#RUN useradd -m nobody && echo "nobody:nobody" | chpasswd && \
RUN adduser nobody sudo
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

COPY requirements.txt .

RUN pip install -r requirements.txt
RUN mkdir -p /app
RUN mkdir -p /app/temp

COPY ./open_modelica_interface.py /app/open_modelica_interface.py
COPY ./models /app/models

RUN chown nobody /app
RUN chown nobody /app/temp

WORKDIR app

USER nobody

ENTRYPOINT ["python3", "open_modelica_interface.py"]


#ENTRYPOINT ["bash", "start.sh"]