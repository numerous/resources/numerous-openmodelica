from image_test.environment import build_run_numerous_image

if __name__ == '__main__':

    build_run_numerous_image(docker_context_folder='.', tag="gcr.io/numerous-mission-control/numerous/open-modelica:latest", push=True, run_image=False)