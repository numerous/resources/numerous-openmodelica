# An integration for running `open-modelica` simulations using the `numerous` platform

## Introduction

This repository contains code to allow you or your team to run open-modelica models from the numerous platform. 

## Requirements

To run the code, it is assumed you have access to a numerous environment, and
that you're somewhat familiar with python.

More specific requirements are:
- Python version >= 3.10
- Installation of `numerous-image-tools` (done automatically
  through `pip install -r requirements.txt`)
- Installation of `numerous-cli` (done automatically as a dependency of
  `numerous-image-tools`)
- For local development you need an installation of open modelica.
- To build the images locally you need docker.




