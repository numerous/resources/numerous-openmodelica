model Bulb
  parameter Real P=0 "Power W";
  parameter Real C=1000 "heat capacity J/K";
  parameter Real h=10 "heat loss W/K";
  parameter Real T_amb=10 "Ambient Temperature C";
  Real P_loss(fixed=false, start=0) "Heat loss W";
  Real T(fixed=true, start=20) "Temperature C";


equation

  P_loss = (T-T_amb)*h;
  der(T) = (P - P_loss)/C;

end Bulb;
