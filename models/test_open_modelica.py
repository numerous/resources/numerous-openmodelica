from OMPython import OMCSessionZMQ

def send(expr):
    print('send: ', expr)
    response = omc.sendExpression(expr)
    print('responded: ', response)
    return response

if __name__ == '__main__':

    omc = OMCSessionZMQ()
    send("getVersion()")
    send("cd()")
    send("loadModel(Modelica)")
    send("loadFile(getInstallationDirectoryPath() + \"/share/doc/omc/testmodels/BouncingBall.mo\")")
    send("instantiateModel(BouncingBall)")

    send("simulate(BouncingBall, stopTime=3.0)")
    send("val(h , 2.0)")