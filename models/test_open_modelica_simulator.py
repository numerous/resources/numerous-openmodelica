from OMSimulator import OMSimulator
import sdf
import matplotlib.pyplot as plt
from copy import copy

def walk_results(results, path="", data_map=[]):
    name = results.name if results.name != "/" else ""
    path = (copy(path) +"." if (not path == "" and name != "") else "") + name

    for data_set in results.datasets:
        data_map.append(path+"."+data_set.name)

    for data_group in results.groups:
        walk_results(data_group, path, data_map)



if __name__ == '__main__':
    oms = OMSimulator()
    oms.setTempDirectory('./temp/')
    model = oms.newModel("model")


    oms.addSystem('model.root', oms.system_sc)


    # instantiate FMUs
    oms.addSubModel('model.root.system1', '../BouncingBall.fmu')

    oms.addConnector("model.root.g", oms.input, oms.signal_type_real)
    #oms.newResources("model.root:data.ssv")
    #oms.export("model", "model_resources.ssp")

    oms.addConnection("model.root.g", "model.root.system1.g")

    # simulation settings
    oms.setResultFile('model', 'results.mat')
    oms.setStopTime('model', 10)
    oms.setFixedStepSize('model', 1e-5)

    oms.instantiate('model')

    oms.initialize('model')

    for i in range(10):
        oms.setReal("model.root.g", 9.0)

        #model.setReal('root.system1.der(h)', 0)

        oms.stepUntil('model', float(i+1)*0.25)

    results = sdf.load(oms.getResultFile('model')[0])

    data_map = []
    walk_results(results, "", data_map)



    oms.terminate('model')


    t_data = results.datasets[0].data

    h_data = results.groups[0].groups[0].groups[0]['h'].data
    g_data = results.groups[0].groups[0].groups[0]['g'].data
    plt.plot(t_data, h_data)
    plt.show()
    oms.delete('model')

