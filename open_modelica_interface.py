import logging
from numerous.image_tools.job import NumerousSimulationJob
from numerous.image_tools.app import run_job

from OMPython import OMCSessionZMQ
from OMSimulator import OMSimulator
import sdf
from copy import copy

logger = logging.getLogger('open-modelica-job')

# Function to walk the results in a .mat file opened with SDF. All variables found will be added to the data map.
def walk_results(results, path="", data_map=[]):
    name = results.name if results.name != "/" else ""
    path = (copy(path) +"." if (not path == "" and name != "") else "") + name

    for data_set in results.datasets:
        data_map.append(path+"."+data_set.name)

    for data_group in results.groups:
        walk_results(data_group, path, data_map)

#  Creates the OpenModelicaJob inheriting the NumerousSimulationJob, which uses the standard work-flow of reading data,
#  stepping the model by calculating new states, outputs, and the next time step, and saves these outputs to numerous.
class OpenModelicaJob(NumerousSimulationJob):
    def __init__(self):
        super(OpenModelicaJob, self).__init__()
        logger.info('Initializing connection to open modelica!')

        self.output_variables = None

        # Create a session with open modelica
        self.omc = OMCSessionZMQ()
        self.send_omc("getVersion()", "OpenModelica version: ")
        self.send_omc("loadModel(Modelica)")

        # Create an OMSimulator session
        self.oms = OMSimulator()
        self.oms.setTempDirectory('./temp/')
        self.oms.newModel("model")
        self.oms.addSystem('model.root', self.oms.system_sc)

        # wrapper function called by NumerousSimulationJob during instanciation of the model.
        self.wrapper = self._wrapper

    #Simple wrapper to print to the logger information on the omc calls.
    def send_omc(self, expr, preprend=""):
        logger.info('omc: '+expr)
        response = self.omc.sendExpression(expr)
        logger.info(preprend+str(response))

        return response

    # Wrapper function called for each component from the numerous system
    def _wrapper(self, component, name, system):

        # Determine the class of the modelica model to use for this component
        modelica_class = component.item_class.split('.')[-1]

        self.modelica_class = modelica_class

        # Assemble the file name for the modelica class
        modelica_file = f"{modelica_class}.mo"

        # Load the modelica class
        load_response = self.send_omc(
            f"loadFile(\"./models/{modelica_file}\")")

        # Instanciate the model
        instanciation_response = self.send_omc(f"instantiateModel({modelica_class})")

        # Create an fmu to be used in the OMSimulator model
        fmu_file = self.send_omc(f"translateModelFMU({modelica_class})")

        # Create a reference for this model/component
        oms_comp_ref = 'model.root.'+name

        # Add the sub model to the OMSimulator model
        self.oms.addSubModel(oms_comp_ref, fmu_file)

        logger.info("Add FMU "+str(fmu_file))

        # Set parameter values for the component in the modelica system
        for param, val in component.constants.items():
            logger.info('Setting '+oms_comp_ref+"."+param+' to: '+str(val))
            self.oms.setReal(oms_comp_ref+"."+param, val)

    def initialize_simulation_system(self):

        # Set align_outputs_to_next_timestep to False, when outputs are to be aligned to the current timestep in the
        # step() method
        self.align_outputs_to_next_timestep = False

        # The modelica model is set to work in relative time
        start = 0
        stop = self.system.end_time - self.system.start_time
        steps = int(stop / self.system.dt) + 1

        # Initialize the OMSimulator model
        self.oms.setResultFile('model', 'results.mat')
        self.oms.setStartTime('model', start)
        self.oms.setStopTime('model', stop)
        self.oms.setFixedStepSize('model', 1e-4)

        self.oms.instantiate('model')
        self.oms.initialize('model')

    def define_outputs(self):
        # Make sure to only do this if output variables not yet defined
        if self.output_variables is None:
            logger.info("Loading output variables")

            # Load the results file and find outputs
            results = sdf.load(self.oms.getResultFile('model')[0])

            self.output_variables = []
            walk_results(results, data_map=self.output_variables)

            logger.info(str(self.output_variables))


    def set_inputs(self):
        # Loop over components and inputs to update modelica model variables according to input values
        for comp_name, comp in self.system.components.items():
            for input_name, input in comp.inputs.items():
                self.oms.setReal("model.root."+comp_name+"."+input_name, input)

    '''
    The step method is called each time the inputs have been read, and must return the next timestep and the outputs,
    which is a dictionary formatted as {tag1: value1, tag2: value2...}  
    '''
    def step(self, t: float = None, dt: float = None) -> (float, dict):

        # Set input variables
        self.set_inputs()

        # Calc the relative time
        t_rel = t - self.system.start_time

        # Ask OMSimulator to step to the current relative time
        self.oms.stepUntil('model', t_rel)

        # Make sure outputs defined
        self.define_outputs()

        # Set the outputs to be saved in numerous platform
        outputs = {'t': t, 't_rel': t_rel}
        outputs.update({var: self.oms.getReal(var)[0] for var in self.output_variables})

        # Finally return the next timestep (t+dt) and the outputs. The outputs are timestamped with the current
        # timestep (t), because of the 'align_outputs_to_next_timestep = False' set above.
        return t + dt, outputs

    # you can save states, by defining the serialize_states. This way you can resume a stateful model.
    def serialize_states(self, t: float = None):
        return True

    def post_run(self, exit_code):
        # Terminate the OMSimulator model and delete it.
        self.oms.terminate('model')
        self.oms.delete('model')


def run_om():
    run_job(numerous_job=OpenModelicaJob(), appname="open-modelica-simulator", model_folder=".")


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    run_om()
